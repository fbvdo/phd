package implementations;

import java.util.ArrayList;

import interfaces.Agent;
import interfaces.Game;
import main.CFG;
import main.Helper;

/**
 * Game framework
 * Agent_OPT.java
 * Optimal agent, always plays the perfect action and has complete knowledge of game dynamics
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class Agent_OPT implements Agent {
	private int id;
	private int gameid;
	
	private double action;
	private int action_slot;
	private double reward;
	private double assets;
	
	public Agent_OPT(int gid){
		gameid = gid;
		id = CFG.aid;
		CFG.aid++;
		assets = CFG.initA;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGameID() {
		return gameid;
	}

	@Override
	/**
	 * Chooses the best possible action by calculating resource dynamics
	 * @param games List of all games being played in the current instance of the simulation
	 * @return action chosen
	 */
	public double playAction(ArrayList<Game> games) {
		double bestact_N = Double.NEGATIVE_INFINITY;
		double bestact_P = Double.NEGATIVE_INFINITY;
		int bestact_slot_P = -1;
		Game game = games.get(gameid);
		
		int i = 0;
		for(double e = CFG.action_min + ((CFG.action_max - CFG.action_min)/CFG.actions); e <= CFG.action_max; e += ((CFG.action_max - CFG.action_min)/CFG.actions)){
			double g = game.getSetup().rg * game.getGameState() * (1 - (game.getGameState()/CFG.maxN));
			double h = game.getSetup().bh * Math.pow(e*CFG.agents, game.getSetup().ah) * Math.pow(game.getGameState(), 1-game.getSetup().ah);
			if(g - h > bestact_N){
				bestact_N = g - h;
			}
			if(g >= h && h > bestact_P){
				bestact_P = h;
				bestact_slot_P = i;
			}
			i++;
		}
		action_slot = bestact_slot_P;
		action = Helper.indexToAction(action_slot);
		assets -= CFG.cost * action;
		return action;
	}

	@Override
	public void receiveReward(double r) {
		reward = r;
		assets += reward;
	}

	@Override
	public void learn(ArrayList<Game> games) {}

	@Override
	public double getActionPlayed() {
		return action;
	}

	@Override
	public double getRewardReceived() {
		return reward;
	}

	@Override
	public double getLongTermValue() {
		return assets;
	}

	@Override
	public ArrayList<Double> getActionHistory() {
		return null;
	}

	@Override
	public Object getAbstractData() {
		return null;
	}

}
