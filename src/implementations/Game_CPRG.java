package implementations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import datastructures.GameSetup;

import main.CFG;
import main.Helper;

import interfaces.Agent;
import interfaces.Game;

/**
 * Game framework
 * Game_CPRG.java
 * Implementation of the common pool resource game (CPRG)
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */

public class Game_CPRG implements Game {
	
	private int id;
	private GameSetup setup;
	private Random rnd;
	
	private ArrayList<Agent> agents;
	private ArrayList<Double> gameStates;
	private ArrayList<Double> ltv;
	private ArrayList<Double> actionhist;
	
	private double N;
	private double G;
	private double H;
	private double X;
	
	ArrayList<Double> gs;
	ArrayList<Double> hs;
	
	ArrayList<Integer> roundVals;
	ArrayList<Double> rgVals;
	
	public Game_CPRG(String atype, GameSetup s){
		setup = s;
		id = CFG.gid;
		CFG.gid++;
		agents = new ArrayList<Agent>();
		for(int a = 0; a < setup.agents; a++){
			agents.add(Helper.initializeAgent(atype, id));
		}
		N = 0.5*s.nmax;
		gameStates = new ArrayList<Double>();
		gameStates.add(N);
		ltv = new ArrayList<Double>();
		actionhist = new ArrayList<Double>();
		rnd = new Random();
		
		gs = new ArrayList<Double>();
		hs = new ArrayList<Double>();
		
		roundVals = new ArrayList<Integer>();
		rgVals = new ArrayList<Double>();
		
		//roundVals = new ArrayList<Integer>(Arrays.asList(100, 500, 1000, 2000, 4000));
		//rgVals = new ArrayList<Double>(Arrays.asList(0.49, 0.48, 0.51, 0.48, 0.475));
		roundVals = new ArrayList<Integer>();
		rgVals = new ArrayList<Double>();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	/**
	 * Coordinates one round of the common pool resource game
	 * -calculates harvest, growth and change of resource
	 * -lets agents learn
	 * @param games The list of all games being played in the current instance of the simulation
	 */
	public void playRound(ArrayList<Game> games) {
		X = 0;
		for(Agent a : agents){
			X += a.playAction(games);
		}
		actionhist.add(X);
		H = calcHarvest(X, N, setup);
		for(Agent a : agents){
			a.receiveReward((a.getActionPlayed()/X) * H);
		}
		
		G = calcGrowth(N, setup);
		N = Math.abs(N - H + G);
		gameStates.add(N);
		for(Agent a : agents){
			a.learn(games);
		}
		double avgassets = 0;
		for(Agent a : agents){
			avgassets += a.getLongTermValue();
		}
		avgassets /= agents.size();
		ltv.add(avgassets);
		
		if(roundVals.size() > 0  && CFG.cround == roundVals.get(0)){
			setup.rg = rgVals.get(0);
			roundVals.remove(0);
			rgVals.remove(0);
		}
	}

	@Override
	public ArrayList<Agent> getAgents() {
		return agents;
	}

	@Override
	public double getGameState() {
		return N;
	}
	
	/**
	 * Calculates the harvest in the current round
	 * @param effort The accumulated actions of all agents
	 * @param resource The current state of the resource
	 * @param gst The parameters of the setup
	 * @return An amount of the resource that is harvested
	 */
	public double calcHarvest(double effort, double resource, GameSetup gst){
		double h = gst.bh * Math.pow(effort, gst.ah) * Math.pow(resource, 1-gst.ah);
		hs.add(h);
		return h;
	}
	
	/**
	 * Calculates the growth in the current round
	 * @param resource The current state of the resource
	 * @param gst The parameters of the setup
	 * @return An amount of the resource that regrows
	 */
	public double calcGrowth(double resource, GameSetup gst){
		double g;
		double rg = 0;
		if(CFG.rgnoise){
			double noise = (rnd.nextDouble() / 100) * CFG.rgnRange;
			boolean sign = rnd.nextBoolean();
			if(sign){
				rg = gst.rg + noise;
			}
			else{
				rg = gst.rg - noise;
			}
			g = rg * resource * (1 - (resource/CFG.maxN));
		}
		else{
			g = gst.rg * resource * (1 - (resource/CFG.maxN));
		}
		gs.add(g);
		return g;
	}

	@Override
	public ArrayList<Double> getGameStateHistory() {
		return gameStates;
	}

	@Override
	public ArrayList<Double> getAverageLongTermValue() {
		return ltv;
	}

	@Override
	public double getCumulativeAction() {
		return X;
	}

	@Override
	public GameSetup getSetup() {
		return setup;
	}

	@Override
	public ArrayList<Double> getActionHistory() {
		return actionhist;
	}

	@Override
	public double getPreviousGameState() {
		return gameStates.get(gameStates.size()-2);
	}

}
