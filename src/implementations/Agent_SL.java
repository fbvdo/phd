package implementations;

import java.util.ArrayList;
import java.util.Random;

import datastructures.History;
import datastructures.QNode;

import main.CFG;
import main.Helper;
import interfaces.Agent;
import interfaces.Game;

/**
 * Game framework
 * Agent_SL.java
 * Sequence learning agent for the CPRG, considers sequences of state-action pairs for evaluation and learning
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class Agent_SL implements Agent {
	
	private int id;
	private int gameid;
	private Random rnd;
	
	private double action;
	private int action_slot;
	private double reward;
	private double assets;
	
	private QNode[] qvals;
	private History data;
	private ArrayList<History> history;
	
	public Agent_SL(int gid){
		gameid = gid;
		id = CFG.aid;
		CFG.aid++;
		assets = CFG.initA;
		rnd = new Random();
		qvals = new QNode[CFG.states];
		for(int i = 0; i < CFG.states; i++){
			qvals[i] = new QNode(CFG.timehorizon*2);
		}
		history = new ArrayList<History>();
		data = new History();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGameID() {
		return gameid;
	}

	@Override
	/**
	 * Chooses an action probabilistically from the Q-tree learned
	 * @param games List of all games being played in the current instance of the simulation
	 * @return action chosen
	 */
	public double playAction(ArrayList<Game> games) {
		data.resource = games.get(gameid).getGameState();
		int state = (int) (data.resource/CFG.maxN*(double)CFG.states);
		data.state = state;
		double sum = 0;
		for(int i = 0; i < qvals[state].subnodes.length; i++){
			sum += Math.pow(Math.E, qvals[state].subnodes[i].retrieve());
		}
		double p = rnd.nextDouble();
		double thresh = 0;
		for(int i = 0; i < qvals[state].subnodes.length; i++){
			thresh += Math.pow(Math.E, qvals[state].subnodes[i].retrieve());
			if(p < thresh/sum){
				action = Helper.indexToAction(i);
				action_slot = i;
				break;
			}
		}

		data.action = action_slot;
		data.effort = action;
		assets -= CFG.cost * action;
		return action;
	}

	@Override
	public void receiveReward(double r) {
		reward = r;
		data.payoff = reward;
		assets += reward;
		data.assets = assets;
	}

	@Override
	/**
	 * Learns and updates the Q-tree from the sequence of actions chosen and the corresponding states of the game
	 * @param games List of all games currently played in this instance of the simulation
	 */
	public void learn(ArrayList<Game> games) {
		data.reward = 0;
		addToSequence(data, history);
		if(CFG.cround >= CFG.timehorizon){
			double N_tm = games.get(gameid).getGameStateHistory()
					.get(games.get(gameid).getGameStateHistory().size()-1-CFG.timehorizon);
			double N_t = games.get(gameid).getGameState();
					
			double A_t = history.get(history.size()-1).assets;
			double A_tm = history.get(history.size()-CFG.timehorizon).assets;
			
			double ego = 0;
			if(A_t - A_tm < 0){
				ego = -CFG.magnitudeQ;
			}
			else if(A_t - A_tm > 0){
				ego = CFG.magnitudeQ;
			}
			double alt = 0;
			if(N_t - N_tm < 0){
				alt = -CFG.magnitudeQ;
			}
			else if(N_t - N_tm > 0){
				alt = CFG.magnitudeQ;
			}
			double rw = (CFG.w * ego + (1 - CFG.w) * alt);
			data.reward = rw;
			
			int state_old = Helper.stateToIndex(N_tm);
			int state_new = Helper.stateToIndex(N_t);
			
			QNode next = qvals[state_old];
			for(int i = 0; i < (CFG.timehorizon*2)-1; i++){
				if(i%2==0){
					int act = history.get(i/2).action;
					next = next.subnodes[act];
					next.frequency += 1;
				}
				else{
					int st = history.get(i/2).state;
					next = next.subnodes[st];
					next.frequency += 1;
				}
			}
			double opt_fut_act = qvals[state_new].findBest();
			next.qval = next.qval + CFG.al * (rw + (CFG.gl * opt_fut_act) - next.qval);
		}
		data = new History();
	}

	@Override
	public double getActionPlayed() {
		return action;
	}

	@Override
	public double getRewardReceived() {
		return reward;
	}

	@Override
	public double getLongTermValue() {
		return assets;
	}
	
	private void addToSequence(History d, ArrayList<History> h){
		h.add(d);
		if(h.size() > CFG.timehorizon){
			h.remove(0);
		}
	}

	@Override
	public ArrayList<Double> getActionHistory() {
		return null;
	}

	@Override
	public Object getAbstractData() {
		return null;
	}

}
