package implementations;

import java.util.ArrayList;
import java.util.Random;

import interfaces.Agent;
import interfaces.Game;
import main.CFG;
import main.Helper;

/**
 * Game framework
 * Agent_METARL.java
 * Agent that learns social norms in the CPRG
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class Agent_METARL implements Agent {
	private int id;
	private int gameid;
	private Random rnd;
	
	private double action;
	private int action_slot;
	private double reward;
	private double assets;
	private double last_assets;
	
	private double[][] qvals;
	private double [] wvals;
	private int ws = 11;
	private double curw = 0.5;
	private int curw_slot = 5;
	private ArrayList<Double> whist;
	
	public Agent_METARL(int gid){
		gameid = gid;
		id = CFG.aid;
		CFG.aid++;
		assets = CFG.initA;
		last_assets = assets;
		rnd = new Random();
		qvals = new double[CFG.states][CFG.actions];
		for(int i = 0; i < qvals.length; i++){
			for(int j = 0; j < qvals[0].length; j++){
				qvals[i][j] = CFG.initQ;
			}
		}
		wvals = new double[ws];
		for(int i = 0; i < wvals.length; i++){
			wvals[i] = CFG.initQ;
		}
		whist = new ArrayList<Double>();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGameID() {
		return gameid;
	}

	@Override
	/**
	 * Chooses an action probabilistically from the QTable learned
	 * @param games List of all games being played in the current instance of the simulation
	 * @return action chosen
	 */
	public double playAction(ArrayList<Game> games) {
		if((CFG.cround-1) % CFG.reassess == 0){
			double sum = 0;
			for(int i = 0; i < wvals.length; i++){
				sum += Math.pow(Math.E, wvals[i]);
			}
			double p = rnd.nextDouble();
			double thresh = 0;
			for(int i = 0; i < wvals.length; i++){
				thresh += Math.pow(Math.E, wvals[i]);
				if(p < thresh/sum){
					curw_slot = i;
					curw = (i/(double)(ws-1));
					break;
				}
			}
			whist.add(curw);
		}
		
		int state = Helper.stateToIndex(games.get(gameid).getGameState());
		double tp = rnd.nextDouble();
		if(tp < CFG.k){
			double sum = 0;
			for(int i = 0; i < qvals[state].length; i++){
				sum += Math.pow(Math.E, qvals[state][i]);
			}
			double p = rnd.nextDouble();
			double thresh = 0;
			for(int i = 0; i < qvals[state].length; i++){
				thresh += Math.pow(Math.E, qvals[state][i]);
				if(p < thresh/sum){
					action = Helper.indexToAction(i);
					action_slot = i;
					break;
				}
			}
		}
		else{
			double bestqv = 0;
			int curi = 0;
			for(int i = 0; i < qvals[state].length; i++){
				if(qvals[state][i] > bestqv){
					bestqv = qvals[state][i];
					curi = i;
				}
			}
			action = Helper.indexToAction(curi);
			action_slot = curi;
		}
		assets -= CFG.cost * action;
		return action;
	}

	@Override
	public void receiveReward(double r) {
		reward = r;
		assets += reward;
	}

	@Override
	/**
	 * Learns and updates the QTable from the action chosen and the state of the game
	 * @param games List of all games currently played in this instance of the simulation
	 */
	public void learn(ArrayList<Game> games) {
		if(CFG.cround % CFG.reassess == 0 && CFG.cround != 0){
			double rww =((assets - last_assets)/CFG.reassess) * CFG.agents;
			int trend;
			if(rww > CFG.initA * 0.15){
				trend = 4;
			}
			else if(rww > CFG.initA * 0.1){
				trend = 3;
			}
			else if(rww > CFG.initA * 0.05){
				trend = 2;
			}
			else if(rww > CFG.initA * 0.01){
				trend = 1;
			}
			else if(rww < -CFG.initA * 0.15){
				trend = -8;
			}
			else if(rww < -CFG.initA * 0.1){
				trend = -6;
			}
			else if(rww < -CFG.initA * 0.05){
				trend = -4;
			}
			else if(rww < 0){
				trend = -2;
			}
			else{
				trend = 0;
			}
			double opt_fut_act = 0;
			for(int i = 0; i < wvals.length;i++){
				if(wvals[i] > opt_fut_act){
					opt_fut_act = wvals[i];
				}
			}
			wvals[curw_slot] = wvals[curw_slot] + CFG.al * (trend + (CFG.gl * opt_fut_act) - wvals[curw_slot]);
			last_assets = assets;
		}
		double N_tm1 = games.get(gameid).getPreviousGameState();
		double N_t = games.get(gameid).getGameState();
		
		double ego = 0;
		if(reward - CFG.cost * action < 0){
			ego = -CFG.magnitudeQ;
		}
		else if(reward - CFG.cost * action > 0){
			ego = CFG.magnitudeQ;
		}
		double alt = 0;
		if(N_t - N_tm1 < 0){
			alt = -CFG.magnitudeQ;
		}
		else if(N_t - N_tm1 > 0){
			alt = CFG.magnitudeQ;
		}
		double rw = (curw * ego + (1 - curw) * alt);

		int state_old = (int) (N_tm1/CFG.maxN*(double)CFG.states);
		int state_new = (int) (N_t/CFG.maxN*(double)CFG.states);
		
		double opt_fut_act = optimalFutureAction(qvals, state_new);	
		qvals[state_old][action_slot] = qvals[state_old][action_slot] + CFG.al * (rw + (CFG.gl * opt_fut_act) - qvals[state_old][action_slot]);
	}

	@Override
	public double getActionPlayed() {
		return action;
	}

	@Override
	public double getRewardReceived() {
		return reward;
	}

	@Override
	public double getLongTermValue() {
		return assets;
	}

	public double optimalFutureAction(double[][] q, int newstate){
		double ofa = 0;
		for(int i = 0; i < q[newstate].length; i++){
			if(q[newstate][i] > ofa){
				ofa = q[newstate][i];
			}
		}
		return ofa;
	}

	@Override
	public ArrayList<Double> getActionHistory() {
		return whist;
	}

	@Override
	public Object getAbstractData() {
		return null;
	}

}
