package implementations;

import java.util.ArrayList;
import java.util.Random;

import main.CFG;
import main.Helper;
import interfaces.Agent;
import interfaces.Game;

/**
 * Game framework
 * Agent_RND.java
 * Implementation of a random agent for the common pool resource game
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */

public class Agent_RND implements Agent {
	
	private int id;
	private int gameid;
	private Random rnd;
	
	private double action;
	private ArrayList<Double> ahist;
	private double reward;
	private double assets;
	
	public Agent_RND(int gid){
		gameid = gid;
		id = CFG.aid;
		CFG.aid++;
		assets = CFG.initA;
		rnd = new Random();
		action = Helper.indexToAction(rnd.nextInt(CFG.actions));
		ahist = new ArrayList<Double>();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGameID() {
		return gameid;
	}

	@Override
	/**
	 * Chooses an action completely at random
	 * @param games List of all games being played in the current instance of the simulation
	 * @return action chosen
	 */
	public double playAction(ArrayList<Game> games) {
		action = Helper.indexToAction(rnd.nextInt(CFG.actions));
		assets -= CFG.cost * action;
		ahist.add(action);
		return action;
	}

	@Override
	public void receiveReward(double r) {
		reward = r;
		assets += reward;
	}

	@Override
	public void learn(ArrayList<Game> games) {}

	@Override
	public double getActionPlayed() {
		return action;
	}

	@Override
	public double getRewardReceived() {
		return reward;
	}

	@Override
	public double getLongTermValue() {
		return assets;
	}

	@Override
	public ArrayList<Double> getActionHistory() {
		return ahist;
	}

	@Override
	public Object getAbstractData() {
		return null;
	}
}
