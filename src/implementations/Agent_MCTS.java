package implementations;

import java.util.ArrayList;
import java.util.Random;

import interfaces.Agent;
import interfaces.Game;
import main.CFG;
import main.Helper;

/**
 * Game framework
 * Agent_MCTS.java
 * Monte Carlo Tree Search agent for the CPRG
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class Agent_MCTS implements Agent {
	
	private int id;
	private int gameid;
	private Random rnd;
	
	private double action;
	private int action_slot;
	private double reward;
	private double assets;
	
	private double[][] qvals;
	private double[][] csa;
	private double[][][] csas;
	private double[][] quct;
	private double[][] uctcsa;
	private double[][][] uctcsas;
	
	private ArrayList<Double> actionhistory;

	public Agent_MCTS(int gid){
		gameid = gid;
		id = CFG.aid;
		CFG.aid++;
		assets = CFG.initA;
		rnd = new Random();
		qvals = new double[CFG.states][CFG.actions];
		quct = new double[CFG.states][CFG.actions];
		csa = new double[CFG.states][CFG.actions+1];
		uctcsa = new double[CFG.states][CFG.actions+1];
		for(int i = 0; i < qvals.length; i++){
			for(int j = 0; j < qvals[0].length; j++){
				qvals[i][j] = CFG.initQ;
				csa[i][j] = 1;
				uctcsa[i][j] = 1;
			}
		}
		for(int k = 0; k < CFG.states; k++){
			csa[k][CFG.actions] = CFG.actions;
			uctcsa[k][CFG.actions] = CFG.actions;
		}
		csas = new double[CFG.states][CFG.actions][CFG.states+1];
		uctcsas = new double[CFG.states][CFG.actions][CFG.states+1];
		for(int l = 0; l < csas.length; l++){
			for(int m = 0; m < csas[0].length; m++){
				for(int n = 0; n < csas[0][0].length; n++){
					csas[l][m][n] = 1;
					uctcsas[l][m][n] = 1;
				}
			}
		}
		for(int o = 0; o < CFG.states; o++){
			for(int p = 0;  p < CFG.actions; p++){
				csas[o][p][CFG.states] = CFG.states;
				uctcsas[o][p][CFG.states] = CFG.states;
			}
		}
		actionhistory = new ArrayList<Double>();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGameID() {
		return gameid;
	}
	
	/**
	 * Recursively simulates roll-outs of the game using Upper Confidence Bounds (UCB) for trees
	 * @param d Current depth within the tree
	 * @param cs Current action explored
	 * @param ca Current state explored
	 * @return
	 */
	public double explore(int d, int cs, int ca){
		if(d == 0){
			int slot = CFG.actions - 1;
			if(uctcsa[cs][CFG.actions] == CFG.actions){
				slot = rnd.nextInt(CFG.actions);
			}
			else{
				double uctmax = 0;
				for(int i = 0; i < CFG.actions; i++){
					double uct = quct[cs][i] + Math.sqrt(2*Math.log(uctcsa[cs][CFG.actions])/uctcsa[cs][i]);
					if(uct > uctmax){
						uctmax = uct;
						slot = i;
					}
				}
			}
			uctcsa[cs][slot]++;
			uctcsa[cs][CFG.actions]++;
			quct[cs][slot] = explore(d+1, cs, slot) * Math.pow(Math.E,qvals[cs][slot]);
			return 0;
		}
		else if(d == CFG.timehorizon){
			return Math.pow(Math.E,qvals[cs][ca]);
		}
		else if(d%2==0){
			int slot = CFG.actions - 1;
			if(uctcsa[cs][CFG.actions] == CFG.actions){
				slot = rnd.nextInt(CFG.actions);
			}
			else{
				double uctmax = 0;
				for(int i = 0; i < CFG.actions; i++){
					double uct = quct[cs][i] + Math.sqrt(2*Math.log(uctcsa[cs][CFG.actions])/uctcsa[cs][i]);
					if(uct > uctmax){
						uctmax = uct;
						slot = i;
					}
				}
			}
			uctcsa[cs][slot]++;
			uctcsa[cs][CFG.actions]++;
			return (csa[cs][slot]/csa[cs][CFG.actions]) * explore(d+1, cs, slot);
		}
		else{
			int slot = CFG.states - 1;
			if(uctcsas[cs][ca][CFG.states] == CFG.states){
				slot =  rnd.nextInt(CFG.states);
			}
			else{
			double uctmax = 0;
				for(int i = 0; i < CFG.states; i++){
					double uct = Math.sqrt(2*Math.log(uctcsas[cs][ca][CFG.states])/uctcsas[cs][ca][i]);
					if(uct > uctmax){
						uctmax = uct;
						slot = i;
					}
				}
			}
			uctcsas[cs][ca][slot]++;
			uctcsas[cs][ca][CFG.states]++;
			return (csas[cs][ca][slot]/csas[cs][ca][CFG.states]) * explore(d+1, slot, ca);
		}
	}

	@Override
	/**
	 * Decides on an action to play using random walks through the Q-table
	 */
	public double playAction(ArrayList<Game> games) {
		int state = Helper.stateToIndex(games.get(gameid).getGameState());
		for(int i = 0; i < CFG.sims; i++){
			explore(0, state, -1);
		}
		double maxuct = 0;
		for(int k = 0; k < CFG.actions; k++){
			if(quct[state][k] > maxuct){
				maxuct = quct[state][k];
				action_slot = k;
			}
		}
		action = Helper.indexToAction(action_slot);	
		actionhistory.add(action);
		assets -= CFG.cost * action;
		return action;
	}

	@Override
	public void receiveReward(double r) {
		reward = r;
		assets += reward;
	}

	@Override
	/**
	 * Learns and updates the QTable from the action chosen and the state of the game
	 * @param games List of all games currently played in this instance of the simulation
	 */
	public void learn(ArrayList<Game> games) {
		double N_tm1 = games.get(gameid).getPreviousGameState();
		double N_t = games.get(gameid).getGameState();
		double rw = Helper.calculateReward(reward, action, N_t, N_tm1);

		int state_old = Helper.stateToIndex(N_tm1);
		int state_new = Helper.stateToIndex(N_t);
		
		csa[state_old][action_slot] += 1;
		csa[state_old][CFG.actions] += 1;
		csas[state_old][action_slot][state_new] += 1;
		csas[state_old][action_slot][CFG.states] += 1;

		double opt_fut_act = optimalFutureAction(qvals, state_new);
		qvals[state_old][action_slot] = qvals[state_old][action_slot] + CFG.al * (rw + (CFG.gl * opt_fut_act) - qvals[state_old][action_slot]);
	}

	@Override
	public double getActionPlayed() {
		return action;
	}

	@Override
	public double getRewardReceived() {
		return reward;
	}

	@Override
	public double getLongTermValue() {
		return assets;
	}

	@Override
	public ArrayList<Double> getActionHistory() {
		return actionhistory;
	}

	@Override
	public Object getAbstractData() {
		return qvals;
	}
	
	/**
	 * Finds the value of the best action an agent can take in the next round
	 * @param q The agent's Q-table
	 * @param newstate The current state of the resource
	 * @return A Q-value
	 */
	public double optimalFutureAction(double[][] q, int newstate){
		double ofa = 0;
		for(int i = 0; i < q[newstate].length; i++){
			if(q[newstate][i] > ofa){
				ofa = q[newstate][i];
			}
		}
		return ofa;
	}
}
