package implementations;

import java.util.ArrayList;
import java.util.Random;

import main.CFG;
import main.Helper;
import interfaces.Agent;
import interfaces.Game;

/**
 * Game framework
 * Agent_RLIS.java
 * Implementation of a reinforcement learning agent with information sharing for the CPRG
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */

public class Agent_RLIS implements Agent{
	
	private int id;
	private int gameid;
	private Random rnd;
	
	private double action;
	private int action_slot;
	private double reward;
	private double assets;
	
	private double[][] qvals;
	private double[][] q_int;
	private double[][] q_ext;
	
	private ArrayList<Double> actionhistory;
	
	public Agent_RLIS(int gid){
		gameid = gid;
		id = CFG.aid;
		CFG.aid++;
		assets = CFG.initA;
		rnd = new Random();
		qvals = new double[CFG.states][CFG.actions];
		q_int = new double[CFG.states][CFG.actions];
		q_ext = new double[CFG.states][CFG.actions];
		for(int i = 0; i < qvals.length; i++){
			for(int j = 0; j < qvals[0].length; j++){
				qvals[i][j] = CFG.initQ;
				q_int[i][j] = CFG.initQ;
				q_ext[i][j] = CFG.initQ;
			}
		}
		actionhistory = new ArrayList<Double>();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGameID() {
		return gameid;
	}

	@Override
	/**
	 * Chooses an action probabilistically from the Q-table learned
	 * @param games List of all games being played in the current instance of the simulation
	 * @return action chosen
	 */
	public double playAction(ArrayList<Game> games) {
		int state = Helper.stateToIndex(games.get(gameid).getGameState());
		
		int iact = 0;
		double isum = 0;
		for(int i = 0; i < q_int[state].length; i++){
			isum += Math.pow(Math.E, q_int[state][i]);
		}
		double ip = rnd.nextDouble();
		double ithresh = 0;
		for(int i = 0; i < q_int[state].length; i++){
			ithresh += Math.pow(Math.E, q_int[state][i]);
			if(ip < ithresh/isum){
				iact = i;
				break;
			}
		}
		int eact = 0;
		double esum = 0;
		for(int i = 0; i < q_ext[state].length; i++){
			esum += Math.pow(Math.E, q_ext[state][i]);
		}
		double ep = rnd.nextDouble();
		double ethresh = 0;
		for(int i = 0; i < q_ext[state].length; i++){
			ethresh += Math.pow(Math.E, q_ext[state][i]);
			if(ep < ethresh/esum){
				eact = i;
				break;
			}
		}
		
		double tp = rnd.nextDouble();
		if(tp < CFG.k){
			double sum = 0;
			for(int i = 0; i < qvals[state].length; i++){
				sum += Math.pow(Math.E, qvals[state][i]);
			}
			double p = rnd.nextDouble();
			double thresh = 0;
			for(int i = 0; i < qvals[state].length; i++){
				thresh += Math.pow(Math.E, qvals[state][i]);
				if(p < thresh/sum){
					action = Helper.indexToAction(i);
					action_slot = i;
					break;
				}
			}
		}
		else{
			double bestqv = 0;
			int curi = 0;
			for(int i = 0; i < qvals[state].length; i++){
				if(qvals[state][i] > bestqv){
					bestqv = qvals[state][i];
					curi = i;
				}
			}
			action = Helper.indexToAction(curi);
			action_slot = curi;
		}
		
		if(CFG.is){
			if(q_ext[state][eact] > qvals[state][action_slot] || q_int[state][iact] > qvals[state][action_slot]){
				if(q_ext[state][eact] > q_int[state][iact]){
					action_slot = eact;
					action = Helper.indexToAction(eact);
				}
				else if(q_int[state][iact] > q_ext[state][eact]){
					action_slot = iact;
					action = Helper.indexToAction(iact);
				}
			}
		}
		assets -= CFG.cost * action;
		actionhistory.add(action);
		return action;
	}

	@Override
	public void receiveReward(double r) {
		reward = r;
		assets += reward;
	}

	@Override
	/**
	 * Learns and updates the Q-table from the action chosen and the state of the game
	 * @param games List of all games currently played in this instance of the simulation
	 */
	public void learn(ArrayList<Game> games) {
		double N_tm1 = games.get(gameid).getPreviousGameState();
		double N_t = games.get(gameid).getGameState();
		double rw = Helper.calculateReward(reward, action, N_t, N_tm1);

		int state_old = Helper.stateToIndex(N_tm1);
		int state_new = Helper.stateToIndex(N_t);
		double opt_fut_act = optimalFutureAction(qvals, state_new);
		qvals[state_old][action_slot] = qvals[state_old][action_slot] + CFG.al * (rw + (CFG.gl * opt_fut_act) - qvals[state_old][action_slot]);
		
		if(CFG.is){
			ArrayList<Agent> intlst = games.get(gameid).getAgents();			
			double intaction = games.get(gameid).getCumulativeAction();
			double intpayoff = 0;
			for(int i = 0; i < intlst.size(); i++){
				intpayoff += intlst.get(i).getRewardReceived();
			}
			rw = Helper.calculateReward(intpayoff, intaction, N_t, N_tm1);
			opt_fut_act = optimalFutureAction(q_int, state_new);
			intaction = Helper.actionToIndex((intaction/(double)CFG.agents));
			intaction = Math.round(intaction);
			q_int[state_old][(int)intaction] = q_int[state_old][(int)intaction] + CFG.al * (rw + (CFG.gl * opt_fut_act) - q_int[state_old][(int)intaction]);
			
			for(int i = 0; i < games.size(); i++){
				if(i == gameid){
					continue;
				}
				if(games.get(i).getGameStateHistory().size() < 2){
					continue;
				}
				N_tm1 = games.get(i).getPreviousGameState();
				N_t = games.get(i).getGameState();
				state_old = Helper.stateToIndex(N_tm1);
				state_new = Helper.stateToIndex(N_t);
				
				ArrayList<Agent> extlst = games.get(i).getAgents();
				double extaction = games.get(i).getCumulativeAction();
				double extpayoff = 0;
				for(int j = 0; j < extlst.size(); j++){
					extpayoff += extlst.get(j).getRewardReceived();
				}
				rw = Helper.calculateReward(extpayoff, extaction, N_t, N_tm1);
				opt_fut_act = optimalFutureAction(q_ext, state_new);
				extaction = Helper.actionToIndex((extaction/CFG.agents));
				extaction = Math.round(extaction);
				q_ext[state_old][(int)extaction] = q_ext[state_old][(int)extaction] + CFG.al * (rw + (CFG.gl * opt_fut_act) - q_ext[state_old][(int)extaction]);
			}
		}
	}

	@Override
	public double getActionPlayed() {
		return action;
	}

	@Override
	public double getRewardReceived() {
		return reward;
	}

	@Override
	public double getLongTermValue() {
		return assets;
	}

	@Override
	public ArrayList<Double> getActionHistory() {
		return actionhistory;
	}

	@Override
	public Object getAbstractData() {
		return qvals;
	}
	
	/**
	 * Finds the value of the best action an agent can take in the next round
	 * @param q The agent's Q-table
	 * @param newstate The current state of the resource
	 * @return A Q-value
	 */
	public double optimalFutureAction(double[][] q, int newstate){
		double ofa = 0;
		for(int i = 0; i < q[newstate].length; i++){
			if(q[newstate][i] > ofa){
				ofa = q[newstate][i];
			}
		}
		return ofa;
	}
}
