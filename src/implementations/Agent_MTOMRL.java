package implementations;

import java.util.ArrayList;
import java.util.Random;

import interfaces.Agent;
import interfaces.Game;
import main.CFG;
import main.Helper;

/**
 * Game framework
 * Agent_MTOMRL.java
 * Full first order Multi-Theory of Mind agent
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class Agent_MTOMRL implements Agent {
	
	private int id;
	private int gameid;
	private Random rnd;
	
	
	private double action;
	private int index_a;
	
	private double state;
	private int index_s;
	
	private double reward;
	private double assets;
	
	private double[][][] b_zero;
	private double[][][][] b_one;
	private double[] c_one;
	private int[] predicted_action;
	private int[] actual_actions;
	
	private double[][][] ptable;
	private ArrayList<Double> actionhistory;
	
	public Agent_MTOMRL(int gid){
		gameid = gid;
		id = CFG.aid;
		CFG.aid++;
		assets = CFG.initA;
		rnd = new Random();
		
		predicted_action = new int[CFG.agents-1];
		actual_actions = new int[CFG.agents-1];
		b_zero = new double[CFG.agents-1][CFG.actions][CFG.states];
		b_one = new double[CFG.agents-1][CFG.agents-1][CFG.actions][CFG.states];
		c_one = new double[CFG.agents-1];

		for(int b = 0; b < b_zero.length; b++){
			for(int s = 0; s < b_zero[0][0].length; s++){
				double bzerosum = 0;
				for(int a = 0; a < b_zero[0].length; a++){
					b_zero[b][a][s] = rnd.nextDouble();
					bzerosum += b_zero[b][a][s];
				}
				for(int a = 0; a < b_zero[0].length; a++){
					b_zero[b][a][s] = b_zero[b][a][s]/bzerosum;
				}
			}
		}
		
		for(int n = 0; n < b_one.length; n++){
			for(int b = 0; b < b_one[0].length; b++){
				for(int s = 0; s < b_one[0][0][0].length; s++){
					double bonesum = 0;
					for(int a = 0; a < b_one[0][0].length; a++){
						b_one[n][b][a][s] = rnd.nextDouble();
						bonesum += b_one[n][b][a][s];
					}
					for(int a = 0; a < b_one[0][0].length; a++){
						b_one[n][b][a][s] = b_one[n][b][a][s]/bonesum;
					}
				}
			}
		}
		
		ptable = new double[CFG.actions][CFG.actions][CFG.states];
		for(int x = 0; x < ptable.length; x++){
			for(int y = 0; y < ptable[0].length; y++){
				for(int z = 0; z < ptable[0][0].length; z++){
					ptable[x][y][z] = CFG.initQ;
				}
			}
		}
		actionhistory = new ArrayList<Double>();
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int getGameID() {
		return gameid;
	}

	@Override
	/**
	 * Chooses an action using first and zero order Theory of Mind
	 * @param games List of all games being played in the current instance of the simulation
	 * @return action chosen
	 */
	public double playAction(ArrayList<Game> games) {
		state = games.get(gameid).getGameState();
		index_s = Helper.stateToIndex(state);	
		
		for(int i = 0; i < predicted_action.length; i++){
			predicted_action[i] = tstar(b_one[i], index_s);
		}		
		index_a = tstar(integrate(b_zero, predicted_action, index_s, c_one), index_s);
		
		action = Helper.indexToAction(index_a);
		actionhistory.add(action);
		assets -= CFG.cost * action;
		return action;
	}

	@Override
	public void receiveReward(double r) {
		reward = r;
		assets += reward;
	}

	@Override
	/**
	 * Learns and updates the QTable from the action chosen and the state of the game
	 * Updates first and zero order beliefs and conficendes with observed actions of other agents
	 * @param games List of all games currently played in this instance of the simulation
	 */
	public void learn(ArrayList<Game> games) {
		ArrayList<Agent> al = games.get(gameid).getAgents();
		int indexcounter = 0;
		for(Agent a : al){
			if(a.getID() != id){
				double action_other = a.getActionPlayed();
				int index_a_other = Helper.actionToIndex(action_other);
				actual_actions[indexcounter] = index_a_other;
				if(predicted_action[indexcounter] == index_a_other){
					c_one[indexcounter] = CFG.tom_gl + (1 - CFG.tom_gl) * c_one[indexcounter];
				}
				else{
					c_one[indexcounter] = (1 - CFG.tom_gl) * c_one[indexcounter];
				}				
				indexcounter++;
			}
		}
		
		double[] gls = new double[CFG.agents-1];
		for(int i = 0; i < gls.length; i++){
			gls[i] = CFG.tom_gl;
		}
		double[][][] updated_zero = integrate(b_zero, actual_actions, index_s, gls);
		for(int x = 0; x < b_zero.length; x++){
			for(int y = 0; y < b_zero[0].length; y++){
				b_zero[x][y][index_s] = updated_zero[x][y][index_s];
			}
		}
		
		double[][][][] updated_one = integrate(b_one, index_a, actual_actions, index_s, CFG.tom_gl);
		for(int x = 0; x < b_one.length; x++){
			for(int y = 0; y < b_one[0].length; y++){
				for(int z = 0; z < b_one[0][0].length; z++){
					b_one[x][y][z][index_s] = updated_one[x][y][z][index_s];
				}
			}
		}
		
		double avgactions = 0;
		for(int i = 0; i < actual_actions.length; i++){
			avgactions += Helper.indexToAction(actual_actions[i]);
		}
		avgactions /= (CFG.agents-1);
		int avgaction = (int)Math.round(Helper.actionToIndex(avgactions));
		
		double N_tm1 = games.get(gameid).getPreviousGameState();
		double N_t = games.get(gameid).getGameState();
		int state_old = Helper.stateToIndex(N_tm1);
		int state_new = Helper.stateToIndex(N_t);
		double rw = Helper.calculateReward(reward, action, N_t, N_tm1);
		
		double opt_fut_act = optimalFutureAction(ptable, state_new);
		ptable[index_a][avgaction][state_old] = ptable[index_a][avgaction][state_old] + CFG.al * (rw + (CFG.gl * opt_fut_act) - ptable[index_a][avgaction][state_old]);
	}

	@Override
	public double getActionPlayed() {
		return action;
	}

	@Override
	public double getRewardReceived() {
		return reward;
	}

	@Override
	public double getLongTermValue() {
		return assets;
	}
	
	public double H(double effort, double resource){
		double h = CFG.bh * Math.pow(effort, CFG.ah) * Math.pow(resource, 1-CFG.ah);
		return h;
	}
	
	public double G(double resource){
		double g = CFG.rg * resource * (1 - (resource/CFG.maxN));
		return g;
	}
	
	/**
	 * Evaluates a particular permutation of actions of all agents
	 * @param a Focal agent action
	 * @param b Integrated beliefs
	 * @param s Current state
	 * @return A qualitative value of all permutations for a
	 */
	public double phi(int a, double[][][] b, int s){	
		int[] chosen_actions = new int[CFG.agents-1];
		int maxdepth = b.length;
		return recursivePhi(a, chosen_actions, b, s, 0, maxdepth);
	}
	
	/**
	 * See previous method
	 * @param a Focal agent action
	 * @param chosen_actions Opponent actions to be evaluated
	 * @param b Integrated beliefs
	 * @param s Current state
	 * @param curdepth Index of opponent action to be evaluated
	 * @param maxdepth Overall opponent actions to be evaluated
	 * @return A qualitative value of all permutations for a
	 */
	public double recursivePhi(int a, int[] chosen_actions, double[][][] b, int s, int curdepth, int maxdepth){		
		if(curdepth == maxdepth){
			double multiplier = 1;
			double actionsum = 0;
			for(int j = 0; j < chosen_actions.length; j++){
				multiplier *= b[j][chosen_actions[j]][s];
				actionsum += Helper.indexToAction(chosen_actions[j]);
			}
			double payoff;
			if(CFG.knownpayoff){
				actionsum += Helper.indexToAction(a);
				payoff = (Helper.indexToAction(a)/actionsum) * H(actionsum, state) - CFG.cost * Helper.indexToAction(a);
			}
			else{
				actionsum = (int)Math.round(((((actionsum / (CFG.agents-1)) - CFG.action_min)/(CFG.action_max - CFG.action_min)) * CFG.actions - 1));
				payoff = ptable[a][(int)actionsum][s];
			}
			return multiplier * payoff;
		}
		else{
			double phisum = 0;
			for(int j = 0; j < CFG.actions; j++){
				chosen_actions[curdepth] = j;
				phisum += recursivePhi(a, chosen_actions, b, s, curdepth+1, maxdepth);
			}
			return phisum;
		}
	}
	
	/**
	 * Selection method for actions
	 * @param b Integrated beliefs
	 * @param s Current state
	 * @return An action
	 */
	public int tstar(double[][][] b, int s){
		if(!CFG.probabilisticselection){
			int best_action = 0;
			double max_phi = Double.NEGATIVE_INFINITY;
			for(int i = 0; i < CFG.actions; i++){
				double phi = phi(i, b, s);
				if(phi > max_phi){
					max_phi = phi;
					best_action = i;
				}
			}
			return best_action;
		}
		else{
			double[] phis = new double[CFG.actions];
			int best_action = 0;
			double asum = 0;
			for(int i = 0; i < CFG.actions; i++){
				phis[i] = Math.pow(Math.E, phi(i, b, s));
				asum += phis[i];
			}
			double rsum = 0;
			double p = rnd.nextDouble();
			for(int i = 0; i < CFG.actions; i++){
				rsum += phis[i];
				if(p < rsum/asum){
					best_action = i;
					break;
				}
			}
			return best_action;
		}
	}
	
	/**
	 * Integrates zero order beliefs with observed actions
	 * @param b Zero order beliefs
	 * @param a Opponent actions taken
	 * @param s Current state
	 * @param c Confidence
	 * @return Integrated beliefs
	 */
	public double[][][] integrate(double[][][] b, int[] a, int s, double c[]){
		double[][][] u = new double[CFG.agents-1][CFG.actions][CFG.states];
		for(int j = 0; j < a.length; j++){
			for(int i = 0; i < b[0].length; i++){
				if(i != a[j]){
					u[j][i][s] = (1.0 - c[j]) * b[j][i][s];
				}
				else{
					u[j][i][s] = (1.0 - c[j]) * b[j][i][s] + c[j];
				}
			}
		}
		return u;
	}
	
	/**
	 * Integrates first order beliefs with observed actions
	 * @param b First order beliefs
	 * @param a Opponent actions taken
	 * @param o Zero order beliefs
	 * @param s Current state
	 * @param c Confidence
	 * @return Integrated beliefs
	 */
	public double[][][][] integrate(double[][][][] b, int a, int[] o, int s, double c){
		double[][][][] u = new double[CFG.agents-1][CFG.agents-1][CFG.actions][CFG.states];
		
		for(int n = 0; n < b.length; n++){
			for(int t = 0; t < b[0].length; t++){
				for(int ij = 0; ij < b[0][0].length; ij++){
					if(t == 0){
						if(ij != a){
							u[n][t][ij][s] = (1.0 - c) * b[n][t][ij][s];
						}
						else{
							u[n][t][ij][s] = (1.0 - c) * b[n][t][ij][s] + c;
						}
					}
					else{
						if(t > n){
							if(ij != o[t]){
								u[n][t][ij][s] = (1.0 - c) * b[n][t][ij][s];
							}
							else{
								u[n][t][ij][s] = (1.0 - c) * b[n][t][ij][s] + c;
							}
						}
						else{
							if(ij != o[t-1]){
								u[n][t][ij][s] = (1.0 - c) * b[n][t][ij][s];
							}
							else{
								u[n][t][ij][s] = (1.0 - c) * b[n][t][ij][s] + c;
							}
						}
					}
				}
			}
		}		
		return u;
	}
	

	@Override
	public ArrayList<Double> getActionHistory() {
		return actionhistory;
	}

	@Override
	public Object getAbstractData() {
		return ptable;
	}
	
	/**
	 * Finds the value of the best action an agent can take in the next round
	 * @param p The agent's Q-table
	 * @param newstate The current state of the resource
	 * @return A Q-value
	 */
	public double optimalFutureAction(double[][][] p, int newstate){
		double ofa = 0;
		for(int i = 0; i < p.length;i++){
			for(int j = 0; j < p[0].length; j++){
				if(p[i][j][newstate] > ofa){
					ofa = p[i][j][newstate];
				}
			}
		}
		return ofa;
	}
}
