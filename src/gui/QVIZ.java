package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JPanel;

import interfaces.Game;
import main.CFG;

/**
 * Game framework
 * QVIZ.java
 * Visualization that draws agent Q-tables where applicable, is jointly toggled with normal visualization
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class QVIZ extends JPanel {
	
	double wd,ht,lx,hx,ly,hy;
	int offset,gap;
	private ArrayList<Game> games;

	public QVIZ(int w, int h){
		setPreferredSize(new Dimension(w, h));
		wd = w;
		ht = h;
		offset = 10;
		gap = 5;
	}
	
	public void setGames(ArrayList<Game> g){
		games = g;
	}
	
	public void paintComponent(Graphics g) {		
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		if(!CFG.mixedpop){
			if(CFG.atype.equals("rlis") || CFG.atype.equals("mcts")){
				drawQT2D(g2);
			}
			else if(CFG.atype.equals("mtomrl") || CFG.atype.equals("mtomgroups") || CFG.atype.equals("notom") || CFG.atype.equals("notomgroups")){
				drawQT3D(g2);
			}
		}
	}
	
	public void drawQT2D(Graphics2D g2){	
		double[][] qt = (double[][])games.get(0).getAgents().get(0).getAbstractData();
		double maxq = 0;
		double minq = 0;
		double absmax = 0;
		for(int a = 0; a < qt.length; a++){
			for(int b = 0; b < qt[0].length; b++){
				if(qt[a][b] > maxq){
					maxq = qt[a][b];
				}
				if(qt[a][b] < minq){
					minq = qt[a][b];
				}
			}
		}
		if(maxq > Math.abs(minq)){
			absmax = maxq;
		}
		else{
			absmax = Math.abs(minq);
		}
		
		g2.setColor(Color.GREEN);
		g2.drawLine(offset, (int)ht/2, (int)wd-offset, (int)ht/2);
		int running = offset;
		int rwidth = (int)((wd-2*offset-CFG.states*gap)/(CFG.states*CFG.actions));
		int magnitude = 0;
		for(int y = 0; y < qt.length; y++){
			for(int x = 0; x < qt[0].length; x++){
				g2.setColor(Color.BLACK);
				magnitude = -(int)((qt[y][x]/absmax)*((ht/2)-offset));
				g2.drawRect(running, (int)ht/2, rwidth, magnitude);
				running += rwidth;
			}
			g2.setColor(Color.RED);
			g2.drawRect(running, (int)ht, gap, (int)-ht);
			running += gap;
		}
		g2.setColor(Color.GREEN);
		g2.drawString("HERE", running, (int)ht/2);
	}
	
	public void drawQT3D(Graphics2D g2){
		double[][][] qt = (double[][][])games.get(0).getAgents().get(0).getAbstractData();
		double maxq = 0;
		double minq = 0;
		double absmax = 0;
		for(int a = 0; a < qt.length; a++){
			for(int b = 0; b < qt[0].length; b++){
				for(int c = 0; c < qt[0][0].length; c++){
					if(qt[a][b][c] > maxq){
						maxq = qt[a][b][c];
					}
					if(qt[a][b][c] < minq){
						minq = qt[a][b][c];
					}
				}
			}
		}
		if(maxq > Math.abs(minq)){
			absmax = maxq;
		}
		else{
			absmax = Math.abs(minq);
		}
		g2.drawString("Min Q "+(int)minq, offset, offset);
		g2.drawString("Max Q "+(int)maxq, offset, offset*2);
		
		for(int i = 0; i < CFG.actions; i++){
			g2.setColor(Color.GREEN);
			g2.drawLine(offset, (int)((i+0.5)*(ht/CFG.actions)), (int)wd-offset, (int)((i+0.5)*(ht/CFG.actions)));
		}
		int rwidth = (int)((wd-2*offset-CFG.states*gap)/(CFG.states*CFG.actions));
		int magnitude = 0;
		for(int stack = 0; stack < qt.length; stack++){
			int running = offset;
			for(int y = 0; y < qt[0][0].length; y++){
				for(int x = 0; x < qt[0].length; x++){
					g2.setColor(Color.BLACK);
					magnitude = -(int)((qt[stack][x][y]/absmax)*((ht/2)/CFG.actions));
					g2.drawRect(running, (int)((stack+0.5)*(ht/CFG.actions)), rwidth, magnitude);
					running += rwidth;
				}
				g2.setColor(Color.RED);
				g2.drawRect(running, (int)ht, gap, (int)-ht);
				running += gap;
			}
			g2.setColor(Color.GREEN);
			g2.drawString("x_i "+stack, running-offset*gap,(int)((stack+0.5)*(ht/CFG.actions)));
		}
	}
}
