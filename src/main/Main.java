package main;

import gui.QVIZ;
import gui.Visualization;
import interfaces.Game;

import java.util.ArrayList;

import javax.swing.JFrame;

import datastructures.GameSetup;

/**
 * Game framework
 * Main.java
 * Runs the main game loop and coordinates data collection and visualization
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class Main {

	/**
	 * @param args Various parameters to be stored in CFG and used to run the simulation, can be customized
	 * 
	 * @throws Exception for IO errors
	 */
	public static void main(String[] args) throws Exception {
		// Optional: run with parameters from file
		//Helper.readSetupFromFile("cfg.txt");
		
		// Uncomment this block if you want to run externally with parameters
		/*CFG.run = Integer.parseInt(args[0]);
		CFG.gtype = args[1];
		CFG.atype = args[2];
		CFG.games = Integer.parseInt(args[3]);
		CFG.agents = Integer.parseInt(args[4]);
		CFG.rg = Double.parseDouble(args[5]);
		CFG.cost = Double.parseDouble(args[6]);
		
		CFG.actions = Integer.parseInt(args[7]);
		CFG.states = Integer.parseInt(args[8]);
		CFG.al = Double.parseDouble(args[9]);
		CFG.gl = Double.parseDouble(args[10]);
		
		CFG.agent_categories = Integer.parseInt(args[11]);
		CFG.deltaT = Integer.parseInt(args[12]);
		CFG.reassess = Integer.parseInt(args[13]);
		CFG.w = Double.parseDouble(args[14]);
		CFG.magnitudeQ = Integer.parseInt(args[15]);*/
		
		CFG.action_max /= CFG.agents;
		CFG.action_min /= CFG.agents;

		ArrayList<Game> games = new ArrayList<Game>();
		GameSetup setup;
		for(int g = 0; g < CFG.games; g++){
			setup = new GameSetup();
			games.add(Helper.initializeGame(CFG.gtype, CFG.atype, setup));
		}
		
		if(CFG.mixedpop){
			for(int i = 0; i < CFG.mpsize; i++){
				games.get(0).getAgents().add(Helper.initializeAgent(CFG.mptype, games.get(0).getID()));
				games.get(0).getAgents().remove(0);
			}
		}
		
		JFrame frame;
		Visualization panel = null;
		if(CFG.gui){
			frame = new JFrame("Game framework: "+CFG.gtype+", "+CFG.atype);
			frame.setSize(1020, 520);
		    frame.setLocation(5, 5);
		    frame.setResizable(false);
		    frame.setVisible(true);
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			panel = new Visualization(1000, 500);
			panel.setGames(games);
			frame.setContentPane(panel);
		}
		
		JFrame q;
		QVIZ p = null;
		if(CFG.gui){
			q = new JFrame("Q-Table: "+CFG.gtype+", "+CFG.atype);
			q.setSize(1520, 820);
		    q.setLocation(400, 220);
		    q.setResizable(false);
		    q.setVisible(true);
		    q.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			p = new QVIZ(1500, 800);
			p.setGames(games);
			q.setContentPane(p);
		}
		
		/*
		 * Main game loop
		 */
		long t_start = System.nanoTime();
		for(int i = 1; i < CFG.rounds; i++){
			for(Game g : games){
				g.playRound(games);
			}
			if(CFG.gui){
				panel.repaint();
				p.repaint();
			}
			CFG.cround++;
		}
		long t_end = System.nanoTime();
		System.out.println("Time elapsed: "+(t_end-t_start)/1000000000.0+" second(s)");

		// Optional: output to file
		String fn = String.format("%s_%dgs_%dag_%srg_%sc_%dac_%sst_%sal_%sgl_%dcat_%ddt_%dre_%sw_%dm_run%d.txt", 				
				CFG.atype, CFG.games, CFG.agents, CFG.rg, CFG.cost, CFG.actions, CFG.states, CFG.al, CFG.gl, 
				CFG.agent_categories, CFG.deltaT, CFG.reassess, CFG.w, CFG.magnitudeQ, CFG.run);
		String fnn = "res_"+fn;
		String fna = "ast_"+fn;
		String fnx = "eff_"+fn;
		//Helper.writeListToFile(fnn, games.get(0).getGameStateHistory());
		//Helper.writeListToFile(fna, games.get(0).getAverageLongTermValue());
		//Helper.writeListToFile(fnx, games.get(0).getActionHistory());
	}

}
