package main;

import implementations.Agent_MCTS;
import implementations.Agent_METARL;
import implementations.Agent_MTOMGROUPS;
import implementations.Agent_MTOMRL;
import implementations.Agent_NOTOM;
import implementations.Agent_NOTOMGROUPS;
import implementations.Agent_OPT;
import implementations.Agent_RLIS;
import implementations.Agent_RND;
import implementations.Agent_SL;
import implementations.Game_CPRG;
import interfaces.Agent;
import interfaces.Game;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

import datastructures.GameSetup;

/**
 * Game framework
 * Helper.java
 * Helper class with various useful global functions
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */

public class Helper {
	
	/**
	 * Initializes a new Game of type gtype and fills it with Agents of type atype
	 * @param gtype Game type / name
	 * @param atype Agent type / name
	 * @return new Game of type gtype with Agents of type atype
	 */
	public static Game initializeGame(String gtype, String atype, GameSetup setup){
		Game game = null;
		if(gtype.equals("cprg")){
			game = new Game_CPRG(atype, setup);
		}
		else {
			System.out.println("Error, no game specified");
		}
		return game;
	}

	/**
	 * Initializes a new Agent of type atype
	 * @param atype Agent type / name
	 * @param gid ID of the game the agent is part of
	 * @return new Agent of type atype
	 */
	public static Agent initializeAgent(String atype, int gid){
		Agent agent = null;
		if(atype.equals("rlis")){
			agent = new Agent_RLIS(gid);
		}
		else if(atype.equals("sl")){
			agent = new Agent_SL(gid);
		}
		else if(atype.equals("rnd")){
			agent = new Agent_RND(gid);
		}
		else if(atype.equals("mtomrl")){
			agent = new Agent_MTOMRL(gid);
		}
		else if(atype.equals("mtomgroups")){
			agent = new Agent_MTOMGROUPS(gid);
		}
		else if(atype.equals("opt")){
			agent = new Agent_OPT(gid);
		}
		else if(atype.equals("metarl")){
			agent = new Agent_METARL(gid);
		}
		else if(atype.equals("mcts")){
			agent = new Agent_MCTS(gid);
		}
		else if(atype.equals("notom")){
			agent = new Agent_NOTOM(gid);
		}
		else if(atype.equals("notomgroups")){
			agent = new Agent_NOTOMGROUPS(gid);
		}
		else{
			System.out.println("Error, no agent specified");
		}
		return agent;
	}
	
	/**
	 * Writes a given list of values to disk
	 * @param filename Name of the file the data is written to
	 * @param vals List of double values to be written to disk
	 * @throws Exception for IO errors
	 */
	public static void writeListToFile(String filename, ArrayList<Double> vals) throws Exception {
		PrintWriter writer = new PrintWriter(filename, "UTF-8");
		for(Double d : vals){
			writer.println(d);
		}
		writer.close();
	}
	
	/**
	 * Writes a given list of arrays to disk
	 * @param filename Name of the file the data is written to
	 * @param vals List of double arrays to be written to disk
	 * @throws Exception for IO errors
	 */
	public static void writeArrayListToFile(String filename, ArrayList<double[]> vals) throws Exception {
		PrintWriter writer = new PrintWriter(filename, "UTF-8");
		for(double[] d : vals){
			String s = "";
			for(int i = 0; i < d.length; i++){
				s = s+d[i]+",";
			}
			writer.println(s.substring(0,s.length()-1));
		}
		writer.close();
	}
	
	/**
	 * Writes a given list of a list of arrays to disk
	 * @param filename Name of the file the data is written to
	 * @param vals List of lists of int arrays
	 * @throws Exception for IO errors
	 */
	public static void writeGroupToFile(String filename, ArrayList<ArrayList<int[]>> vals) throws Exception {
		PrintWriter writer = new PrintWriter(filename, "UTF-8");
		for(ArrayList<int[]> l : vals){
			for(int[] d : l){
				String s = "";
				for(int i = 0; i < d.length; i++){
					s = s+d[i]+",";
				}
				if(s.length()!=0){
					writer.println(s.substring(0,s.length()-1));
				}
				else{
					writer.println("");
				}
				
			}
			
		}
		writer.close();
	}
	
	/**
	 * Reads framework parameters from a file and stores them in CFG
	 * @param fn Name of the file parameters are read from
	 * @throws Exception for IO errors
	 */
	public static void readSetupFromFile(String fn) throws Exception{
		FileReader fr = new FileReader(fn);
		BufferedReader br = new BufferedReader(fr);
		String line;
		while((line = br.readLine()) != null){
			StringTokenizer st = new StringTokenizer(line, ",");
			String fname = st.nextToken();
			String fvalue = st.nextToken();
			if(fname.equals("run")){
				CFG.run = Integer.parseInt(fvalue);
			}
			if(fname.equals("gtype")){
				CFG.gtype = fvalue;
			}
			if(fname.equals("atype")){
				CFG.atype = fvalue;
			}
			if(fname.equals("rounds")){
				CFG.rounds = Integer.parseInt(fvalue);
			}
			if(fname.equals("games")){
				CFG.games = Integer.parseInt(fvalue);
			}
			if(fname.equals("agents")){
				CFG.agents = Integer.parseInt(fvalue);
			}
			if(fname.equals("gui")){
				CFG.gui = Boolean.parseBoolean(fvalue);
			}
			/*------------------------------------------*/
			if(fname.equals("rg")){
				CFG.rg = Double.parseDouble(fvalue);
			}
			if(fname.equals("cost")){
				CFG.cost = Double.parseDouble(fvalue);
			}
			if(fname.equals("actions")){
				CFG.actions = Integer.parseInt(fvalue);
			}
			if(fname.equals("states")){
				CFG.states = Integer.parseInt(fvalue);
			}
			/*------------------------------------------*/
			if(fname.equals("initQ")){
				CFG.initQ = Double.parseDouble(fvalue);
			}
			if(fname.equals("magnitudeQ")){
				CFG.magnitudeQ = Integer.parseInt(fvalue);
			}
			if(fname.equals("w")){
				CFG.w = Double.parseDouble(fvalue);
			}
			if(fname.equals("is")){
				CFG.is = Boolean.parseBoolean(fvalue);
			}
			/*------------------------------------------*/
			if(fname.equals("agent_categories")){
				CFG.agent_categories = Integer.parseInt(fvalue);
			}
			if(fname.equals("deltaT")){
				CFG.deltaT = Integer.parseInt(fvalue);
			}
		}
		br.close();
	}
	
	/**
	 * Calculates the average of values in a list
	 * @param vals List of double values
	 * @return Average of all values in vals
	 */
	public static double avgDouble(ArrayList<Double> vals){
		int sz = vals.size();
		double avg = 0;
		for(Double d : vals){
			avg += d;
		}
		return (avg/sz);
	}
	
	/**
	 * Calculates the average of values in a list
	 * @param vals List of long values
	 * @return Average of all values in vals
	 */
	public static long avgLong(ArrayList<Long> vals){
		int sz = vals.size();
		double avg = 0;
		for(Long d : vals){
			avg += d;
		}
		return (long)(avg/sz);
	}
	
	/**
	 * Calculates the reward to update Q-values for various learning agents
	 * @param reward The pay-off an agent has received
	 * @param action The action an agent has taken
	 * @param N_t The state of the resource in the current round
	 * @param N_tm1 The state of the resource in the previous round
	 * @return A qualitative indicator of agent performance
	 */
	public static double calculateReward(double reward, double action, double N_t, double N_tm1){
		double ego = 0;
		if(reward - CFG.cost * action < 0){
			ego = -CFG.magnitudeQ;
		}
		else if(reward - CFG.cost * action > 0){
			ego = CFG.magnitudeQ;
		}
		double alt = 0;
		if(N_t - N_tm1 < 0){
			alt = -CFG.magnitudeQ;
		}
		else if(N_t - N_tm1 > 0){
			alt = CFG.magnitudeQ;
		}
		double rw = (CFG.w * ego  + (1 - CFG.w) * alt);
		return rw;
	}
	
	/**
	 * Converts a continuous action into a discretized index, the range of which is given in CFG
	 * @param a The action
	 * @return An index
	 */
	public static int actionToIndex(double a){
		return (int)(((a - CFG.action_min)/(CFG.action_max - CFG.action_min)) * CFG.actions - 1);
	}
	
	/**
	 * Converts a discretized index into a continuous action, the range of which is given in CFG
	 * @param i The index
	 * @return An action
	 */
	public static double indexToAction(int i){
		return ((i+1)/(double)CFG.actions) * (CFG.action_max - CFG.action_min) + CFG.action_min;
	}
	
	/**
	 * Converts a continuous state into discretized index, the range of which is given in CFG
	 * @param n The state
	 * @return An index
	 */
	public static int stateToIndex(double n){
		return (int)(n/CFG.maxN*(double)CFG.states);
	}
}
