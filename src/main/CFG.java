package main;

/**
 * Game framework
 * CFG.java
 * Configuration file to store global parameters
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */

public class CFG {
	public static int run = 1;
	public static String gtype = "cprg";
	public static String atype = "rlis";
	
	public static int rounds = 5000;
	public static int cround = 1;
	public static int games = 1;
	public static int gid = 0;
	public static int agents = 10;
	public static int aid = 0;
	public static boolean gui = true;
	
	public static boolean mixedpop = false; // only in single games
	public static String mptype = "rnd";
	public static int mpsize = 1;
	
	/*
	 * CPRG
	 */
	public static double maxN = 1000.0;
	public static double ah = 0.4;
	public static double bh = 0.35;
	public static double rg = 0.5;
	public static boolean rgnoise = false;
	public static double rgnRange = 1.0; // each 1.0 changes growth rate by 0.01
	
	public static double action_max = 500.0;
	public static double action_min = 100.0;
	
	public static double initA = 1000.0;
	public static double cost = 0.5;
		
	/*
	 * Learning
	 */
	public static int actions = 20;
	public static int states = 5;
	public static double al = 0.5; // learning rate
	public static double gl = 0.1; // future discount factor
	public static double k = 1.0; // Learning temperature (exploration vs exploitation)
	
	public static double initQ = 1.0;
	public static int magnitudeQ = 5;
	public static double w = 0.5; // emphasis on egoistic/altruistic component
	public static boolean is = false;
	
	/*
	 * Theory of mind
	 */
	
	public static double tom_gl = 0.5;
	public static int agent_categories = 4;
	public static int deltaT = 10;
	public static int reassess = 100;
	public static boolean knownpayoff = false; //false
	public static boolean probabilisticselection = true; //true
	
	/*
	 * Sequence learning/MCTS
	 */
	public static int timehorizon = 3; // SL: must be odd to end with an action
	public static int sims = 1000;
}
