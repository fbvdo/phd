package datastructures;

/**
 * Game framework
 * History.java
 * Data structure to contain all measured variables in a round
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class History {
	
	public double resource;
	public int state;
	public double effort;
	public int action;
	
	public double payoff;
	public double reward;
	public double assets;

	public History(){
		
	}
}
