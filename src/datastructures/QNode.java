package datastructures;

import java.util.Random;

import main.CFG;

/**
 * Game framework
 * QNode.java
 * Data structure utilized by the sequence learning agent to build a Q-tree.
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class QNode {
	public enum Type{
		STATE,ACTION,LEAF;
	};
	
	public QNode[] subnodes;
	public Type type;
	public double qval;
	public int frequency;
	public int depth;
	
	Random rnd;
	
	public QNode(int th){
		th -= 1;
		depth = th;
		rnd = new Random();
		if(th == 0){
			type = Type.LEAF;
			qval = rnd.nextDouble();
			frequency = 1;
		}
		else if(th%2 == 0){
			type = Type.ACTION;
			subnodes = new QNode[CFG.states];
			for(int i = 0; i < CFG.states; i++){
				subnodes[i] = new QNode(th);
			}
			frequency = (int) Math.pow(CFG.actions*CFG.states, th/2);
		}
		else{
			int cnt = 0;
			type = Type.STATE;
			subnodes = new QNode[CFG.actions];
			for(int i = 0; i < CFG.actions; i++){
				subnodes[i] = new QNode(th);
				cnt += subnodes[i].frequency;
			}
			frequency = cnt;
		}
	}
	
	/**
	 * Find the subnode with the highest accumulated Q-value
	 * @return A subnode
	 */
	public double findBest(){
		double best = 0;
		for(QNode q : subnodes){
			if(q.type == Type.LEAF){
				if(q.qval > best){
					best = q.qval;
				}
			}
			else{
				if(q.findBest() > best){
					best = q.findBest();
				}
			}
		}
		return best;
	}
	
	/**
	 * Find the subnode with the lowest accumulated Q-value
	 * @return A subnode
	 */
	public double findWorst(){
		double best = 0;
		for(QNode q : subnodes){
			if(q.type == Type.LEAF){
				if(q.qval < best){
					best = q.qval;
				}
			}
			else{
				if(q.findBest() < best){
					best = q.findBest();
				}
			}
		}
		return best;
	}
	
	/**
	 * Evaluates all subnodes recursively
	 * @return The accumulated weighted Q-value for all subnodes
	 */
	public double retrieve(){
		if(type == Type.LEAF){
			return qval;
		}
		else{
			double[] qv = new double[subnodes.length];
			for(int i = 0; i < subnodes.length; i++){
				qv[i] = subnodes[i].retrieve();
			}			
			double val = 0;
			for(int i = 0; i < subnodes.length; i++){
				val += ((double)subnodes[i].frequency/(double)frequency) * qv[i];
			}
			qval = val;
			return val;
		}
	}
}
