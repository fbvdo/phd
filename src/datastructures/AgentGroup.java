package datastructures;

import java.util.ArrayList;

import interfaces.Agent;

/**
 * Game framework
 * AgentGroup.java
 * Data structure to contain a group of agents
 * 
 * @author Friedrich Burkhard von der Osten
 * @version 3.0
 */
public class AgentGroup {
	
	private ArrayList<Agent> agents;
	
	public AgentGroup(){
		agents = new ArrayList<Agent>();
	}
	
	public void add(Agent a){
		agents.add(a);
	}
	
	public ArrayList<Agent> getAgents(){
		return agents;
	}
	
	/**
	 * Calculate the average action the agents in the group have taken
	 * @return The average action of all agents in the group
	 */
	public double calcAvgAction(){
		double asum = 0;
		for(Agent a : agents){
			asum += a.getActionPlayed();
		}
		asum /= agents.size();
		return asum;
	}
}
